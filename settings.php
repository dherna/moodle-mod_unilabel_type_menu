<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * unilabel type menu
 *
 * @package     unilabeltype_menu
 * @author      Andreas Grabs <info@grabs-edv.de>
 * @copyright   2018 onwards Grabs EDV {@link https://www.grabs-edv.de}
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

$page = new admin_settingpage('unilabeltype_menu', get_string('pluginname', 'unilabeltype_menu'));

$menusettings = array();

$menusettings[] = new admin_setting_configcheckbox('unilabeltype_menu/active',
    get_string('active'),
    '',
    true);

$numbers = array_combine(range(1, 6), range(1, 6));
$menusettings[] = new admin_setting_configselect('unilabeltype_menu/columns',
    get_string('default_columns', 'unilabeltype_menu'),
    '',
    4,
    $numbers
);

$numbers = array_combine(range(100, 600, 50), range(100, 600, 50));
$numbers = array(0 => get_string('autoheight', 'unilabeltype_menu')) + $numbers;
$menusettings[] = new admin_setting_configselect('unilabeltype_menu/height',
    get_string('default_height', 'unilabeltype_menu'),
    get_string('height_help', 'unilabeltype_menu'),
    300,
    $numbers
);

$menusettings[] = new admin_setting_configcheckbox('unilabeltype_menu/showintro',
    get_string('default_showintro', 'unilabeltype_menu'),
    '',
    false
);

$menusettings[] = new admin_setting_configcheckbox('unilabeltype_menu/usemobile',
    get_string('default_usemobile', 'unilabeltype_menu'),
    '',
    true
);

foreach ($menusettings as $setting) {
    $page->add($setting);
}

$settingscategory->add($page);
